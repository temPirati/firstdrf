from django.urls import path
from .views import CustomUserCreate, UserList, UserDetail

app_name = 'user'

urlpatterns = [
    path('', UserList.as_view(), name='listusercreate'),
    path('<int:pk>', UserDetail.as_view(), name='detailusercreate'),
    path('register/', CustomUserCreate.as_view(), name="create_user"),
]