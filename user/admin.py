from django.contrib import admin

from user.models import User
from django.contrib.auth.admin import UserAdmin


class UserAdminConfig(UserAdmin):
    model = User
    serach_fields = ('username', 'email', 'first_name', 'last_name')
    ordering = ('-created',)
    list_display = ('username', 'id', 'first_name', 'email', 'is_staff', 'is_active',)

    fieldsets = (
        (None, {'fields': ('username', 'email', 'first_name', 'last_name',)}),
        ('Premissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'first_name', 'last_name', 'password1', 'password2', 'is_active',
                       'is_staff', 'is_superuser')}),
    )


admin.site.register(User, UserAdminConfig)