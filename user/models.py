from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager, PermissionsMixin)


class CustomAccountManager(BaseUserManager):

    def create_superuser(self, username, email, first_name, password, **other_fields):

        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)
        other_fields.setdefault('is_active', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser duhet te jete is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser duhet te jete is_superuser=True.')

        return self.create_user(username, email, first_name, password, **other_fields)

    def create_user(self, username, email, first_name, password, **other_fields):

        if not username:
            raise ValueError('Duhet te vendosesh nje username')

        if not email:
            raise ValueError('Duhet te vendosesh nje email')

        email = self.normalize_email(email)
        user = self.model(username=username, email=email, first_name=first_name, **other_fields)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):

    username = models.CharField(max_length=150, unique=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    about = models.TextField()
    # image = models.ImageField(
    #     verbose_name= "image",
    #     help_text="Upload a property image",
    #     upload_to="images/",
    #     default="images/default.png",
    # )
    # alt_text = models.CharField(
    #     verbose_name="Alturnative text",
    #     help_text="Please add alturnative text",
    #     max_length=255,
    #     null=True,
    #     blank=True,
    # )
    email = models.EmailField(unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = CustomAccountManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'email']

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

