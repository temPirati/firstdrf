from django.urls import path
from .views import PostList, PostDetail


app_name = 'blog_api'

urlpatterns = [
    path('<int:pk>', PostDetail.as_view(), name='detailcreate'),
    path('', PostList.as_view(), name='listcreate'),
    # path('users/', UserList.as_view(), name='listusercreate'),
    # path('users/<int:pk>', UserDetail.as_view(), name='detailusercreate'),
]