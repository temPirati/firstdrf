from rest_framework import generics
from blog.models import Post
from .serializers import PostSerializer
from .permissions import IsOwnerOrReadOnly
from user.models import User
from rest_framework.permissions import IsAuthenticatedOrReadOnly


class PostList(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Post.postobjects.all()
    serializer_class = PostSerializer


class PostDetail(generics.RetrieveUpdateDestroyAPIView, IsOwnerOrReadOnly):
    permission_classes = [IsOwnerOrReadOnly]
    queryset = Post.objects.all()
    serializer_class = PostSerializer

# class UserList(generics.ListAPIView):
#
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#
#
# class UserDetail(generics.RetrieveDestroyAPIView):
#
#     queryset = User.objects.all()
#     serializer_class = UserSerializer